MONGO_HOST = 'localhost'
MONGO_PORT = 27017
    
X_DOMAINS = '*'
X_HEADERS = ['Content-Type']

CACHE_CONTROL = 'no-cache'
CACHE_EXPIRES = 0

IF_MATCH = False
RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PATCH', 'PUT', 'DELETE']
XML = False
EMBEDDING = True

## ## ## ## ##
## animes definition
## ## ## ## ##

animes_schema = {
    # 'identifier' : String : Unique identifier for the anime.
    # It's usually the Anime name without spaces/special characters.
    # We'll use that value to get the anime.
    'identifier': {
    	'type': 'string',
    	'required': True,
    	'unique': True,
    	'minlength': 3,
    },
    # 'title' : String : Title of the anime.
    'title': {
        'type': 'string',
        'required': True,
    },
    'cover_url': {
        'type': 'string',
        'regex': '^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$'
    },
    # 'synopsis' : String : Synopsis of the anime.
    'synopsis': {
        'type': 'string',
        'required': True
    },
    # 'genres' : List[String] : Genres of the anime.
    'genres': {
        'type': 'list',
        'allowed': ["Action","Adventure","Cars","Comedy","Dementia","Demons","Drama","Ecchi","Fantasy","Game","Harem","Hentai","Historical","Horror","Josei","Kids","Magic","Martial Arts","Mecha","Military","Music","Mystery","Parody","Police","Psychological","Romance","Samurai","School","Sci-Fi","Seinen","Shoujo","Shoujo Ai","Shounen","Shounen Ai","Slice of Life","Space","Sports","Super Power","Supernatural","Thriller","Vampire","Yaoi","Yuri"]
    },
    # 'status' : String : Status of the anime.
    'status': {
        'type': 'string',
        'allowed': ["finished airing","currently airing","not yet aired"]
    },
    # 'episodes' : integer : Total episodes of the anime.
    'episodes': {
        'type': 'integer',
        'min': -1
    }
}

animes = {
    'additional_lookup': {
        'url': 'regex("[\w]+")',
        'field': 'identifier'
    },
    
    'schema': animes_schema
}

## ## ## ## ##
## watchlist definition
## ## ## ## ##

watchlist_schema = {
    'id': {
        'type': 'string',
        'minlength': 4,
        'maxlength': 16,
        'unique': True,
        'required': True
    }
}

watchlist = {
    'additional_lookup': {
        'url': 'regex("[\w]+")',
        'field': 'id'
    },
    
    'schema': watchlist_schema
}

## ## ## ## ##
## users definition
## ## ## ## ##

users_schema = {
    'username': {
        'type': 'string',
        'minlength': 4,
        'maxlength': 16,
        'required': True,
        'unique': True
    },
    'watchlist': {
        'type': 'objectid',
        'data_relation': {
            'resource': 'watchlist',
            'field': '_id',
            'embeddable': True
        },
        'nullable': True,
        'default': None
    },
    'animecount': {
        'type': 'integer',
        'default': 0
    }

}

users = {
    'additional_lookup': {
        'url': 'regex("[\w]+")',
        'field': 'username'
    },
    
    'schema': users_schema
}

## ## ## ## ##
## DOMAIN declaration
## ## ## ## ##

DOMAIN = {
    'animes': animes
}