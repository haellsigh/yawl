import os
from eve import Eve

if __name__ == '__main__':
    if 'PORT' in os.environ:
        mPort = int(os.environ.get('PORT', 8080))
        mHost = '0.0.0.0'
    else:
        port = 6112
        host = '127.0.0.1'

    app = Eve()
    app.run(host=mHost, port=mPort)